package main

import (
	"dnaim/config"
	"dnaim/internal/pkg/bot/connection"
)

func main() {
	config := config.NewConfig()
	_ = connection.NewTelegramBot(config)
}
