package config

type PostgresConfig struct {
	Host            string `json:"host"`
	Port            string `json:"port"`
	User            string `json:"user"`
	Password        string `json:"password"`
	DBName          string `json:"dbName"`
	SSLMode         string `json:"sslMode"`
	PgDriver        string `json:"pgDriver"`
	ConnMaxIdleTime int    `json:"connMaxIdleTime"`
	MaxOpenConns    int    `json:"maxOpenConns"`
}

func (sc *PostgresConfig) ParseConfig() error {
	return nil
}
