package model

import "time"

type SetInterviewParams struct {
	Slot              *string    `db:"name"`
	DateInterview     *time.Time `db:"date_interview"`
	CandidateName     *string    `db:"candidate_name"`
	Job               *string    `db:"job"`
	CandidateChatName *string    `db:"candidate_chat_name"`
}

type CheckInterviewParams struct {
	Job               *string `db:"job"`
	CandidateChatName *string `db:"candidate_chat_name"`
}

type CheckInterviewResult struct {
	Slot          *string    `db:"slot"`
	DateInterview *time.Time `db:"date_interview"`
	CandidateName *string    `db:"candidate_name"`
}

type ChangeInterviewDateParams struct {
	DateInterview     *time.Time `db:"date_interview"`
	Slot              *string    `db:"name"`
	CandidateChatName *string    `db:"candidate_chat_name"`
}

type GetInterviewByDateParams struct {
	ChatID            *int64  `db:"chat_id"`
	Status            *string `db:"status"`
	InterviewID       *int64  `db:"id"`
	Slot              *string `db:"slot"`
	Interviewer       *string `db:"interviewer"`
	CandidateName     *string `db:"candidate_name"`
	CandidateChatName *string `db:"candidate_chat_name"`
	DateInterview     *string `db:"date_interview"`
	IsHeld            *bool   `db:"is_held"`
	IsApprove         *bool   `db:"is_approve"`
	Job               *string `db:"job"`
	Description       *string `db:"description"`
}
