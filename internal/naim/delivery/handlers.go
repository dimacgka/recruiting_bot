package naim

import (
	"dnaim/config"
	naimInterface "dnaim/internal/naim/interface"
	"dnaim/internal/naim/model"
	"fmt"
	tele "gopkg.in/telebot.v3"
	"log"
	"strings"
	"time"
)

const (
	ok       = "Хорошо!"
	no       = "Нет!"
	yes      = "Да!"
	disagree = "Не готов!"
	layout   = "02.01.2006"
)

var slots = []string{"13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00"}
timeID := map[string]int64{
	"12:30": 1,
	"12:50": 1,
	"13:30": 2,
	"13:50": 2,
	"14:30": 3,
	"14:50": 3,
	"15:30": 4,
	"15:50": 4,
	"16:30": 5,
	"16:50": 5,
	"17:30": 6,
	"17:50": 6,
	"18:30": 7,
	"18:50": 7,
	}

type NaimHandler struct {
	useCase naimInterface.NaimUseCaseI
	kernel  *tele.Bot
	cfg     config.Config
}

func NewNaimHandler(useCase naimInterface.NaimUseCaseI, kernel *tele.Bot, cfg config.Config) NaimHandler {
	nh :=  NaimHandler{
		useCase: useCase,
		kernel:  kernel,
		cfg:     cfg,
	}

	go nh.sendNotification()

	return nh
}

func (nh *NaimHandler) sendNotification() {
	ticker := time.Tick(30 * time.Second)

	for range ticker {
		location, _ := time.LoadLocation("Europe/Moscow")
		currentTime := time.Now().In(location).Format("15:04")

		switch currentTime {
		case "12:30", "12:50", "13:30", "13:50", "14:30", "14:50", "15:30", "15:50":
			go myFunc()
		}
	}

	time.Sleep(24 * time.Hour)
}

func (nh *NaimHandler) createMessageNotification(date *time.Time, slotID *int64)

func (nh *NaimHandler) Attach() {
	//--------------------1 level----------------------
	nh.kernel.Handle("/start", func(ctx tele.Context) error {
		userChatName := ctx.Chat().Username
		message := fmt.Sprintf("Здравствуйте, %s. Ответьте пожалуйста на 5 вопросов, прежде чем назначить дату и время собеседования", userChatName)

		btn1 := tele.ReplyButton{Text: ok}
		btn2 := tele.ReplyButton{Text: no}

		keys := [][]tele.ReplyButton{
			{btn1, btn2},
		}

		replyKeyboard := tele.ReplyMarkup{
			ReplyKeyboard:  keys,
			ResizeKeyboard: true,
		}

		err := ctx.Send(message, &replyKeyboard)
		if err != nil {
			log.Fatal("Error in sending message!")
			return err
		}

		//--------------------2 level----------------------
		nh.kernel.Handle(tele.OnText, func(ctx tele.Context) error {
			answer := strings.Replace(ctx.Text(), "\f", "", 1)
			if answer == ok {
				message = "1. Введите пожалуйста Ваше ФИО"
				err := ctx.Send(message)
				if err != nil {
					log.Fatal("Error in sending message!")
					return err
				}

				//--------------------3 level----------------------
				nh.kernel.Handle(tele.OnText, func(ctx tele.Context) error {
					name := strings.Replace(ctx.Text(), "\f", "", 1)
					message = "2. На какую вакансию вы отклинулись?"
					btn1 = tele.ReplyButton{Text: "Backend"}
					btn2 = tele.ReplyButton{Text: "Frontend"}
					btn3 := tele.ReplyButton{Text: "Оператор"}
					btn4 := tele.ReplyButton{Text: "Ассистент"}

					keys = [][]tele.ReplyButton{
						{btn1, btn2},
						{btn3, btn4},
					}
					replyKeyboard = tele.ReplyMarkup{
						ReplyKeyboard:  keys,
						ResizeKeyboard: true,
					}

					err := ctx.Send(message, &replyKeyboard)
					if err != nil {
						log.Fatal("Error in sending message!")
						return err
					}

					//--------------------4 level----------------------
					nh.kernel.Handle(tele.OnText, func(ctx tele.Context) error {
						job := strings.Replace(ctx.Text(), "\f", "", 1)

						resultCheck, err := nh.useCase.CheckInterview(
							&model.CheckInterviewParams{
								Job:               &job,
								CandidateChatName: &userChatName,
							})
						if err != nil {
							return err
						}
						if resultCheck != nil {
							message = fmt.Sprintf("По этой вакансии вы уже записаны на собеседование %s в %s\nВы также можете изменить дату, нажав на команду /change_date_interview", *resultCheck.DateInterview, *resultCheck.DateInterview)
							err = ctx.Send(message, &replyKeyboard)
							if err != nil {
								log.Fatal("Error in sending message!")
								return err
							}
						}

						message = "3. Вопрос?"
						btn1 = tele.ReplyButton{Text: "1"}
						btn2 = tele.ReplyButton{Text: "2"}
						btn3 = tele.ReplyButton{Text: "3"}
						btn4 = tele.ReplyButton{Text: "4"}

						keys = [][]tele.ReplyButton{
							{btn1, btn2},
							{btn3, btn4},
						}
						replyKeyboard = tele.ReplyMarkup{
							ReplyKeyboard:  keys,
							ResizeKeyboard: true,
						}

						err = ctx.Send(message, &replyKeyboard)
						if err != nil {
							log.Fatal("Error in sending message!")
							return err
						}

						//--------------------5 level----------------------
						nh.kernel.Handle(tele.OnText, func(ctx tele.Context) error {
							question3 := strings.Replace(ctx.Text(), "\f", "", 1)
							fmt.Println(question3)
							message = "4. Вопрос?"
							btn1 = tele.ReplyButton{Text: "1"}
							btn2 = tele.ReplyButton{Text: "2"}
							btn3 = tele.ReplyButton{Text: "3"}
							btn4 = tele.ReplyButton{Text: "4"}

							keys = [][]tele.ReplyButton{
								{btn1, btn2},
								{btn3, btn4},
							}
							replyKeyboard = tele.ReplyMarkup{
								ReplyKeyboard:  keys,
								ResizeKeyboard: true,
							}

							err := ctx.Send(message, &replyKeyboard)
							if err != nil {
								log.Fatal("Error in sending message!")
								return err
							}

							//--------------------6 level----------------------
							nh.kernel.Handle(tele.OnText, func(ctx tele.Context) error {
								question4 := strings.Replace(ctx.Text(), "\f", "", 1)
								fmt.Println(question4)
								message = "5. Готовы ли вы работать в офисе.\n<b>Мы не рассматириваем кандидатов на удаленку!</b>"
								btn5 := tele.ReplyButton{Text: yes}
								btn6 := tele.ReplyButton{Text: disagree}

								keys2 := [][]tele.ReplyButton{
									{btn5, btn6},
								}
								replyKeyboard2 := tele.ReplyMarkup{
									ReplyKeyboard:  keys2,
									ResizeKeyboard: true,
								}

								err := ctx.Send(message, &tele.SendOptions{ParseMode: tele.ModeHTML}, &replyKeyboard2)
								if err != nil {
									log.Fatal("Error in sending message!")
									return err
								}

								//--------------------7 level----------------------
								nh.kernel.Handle(tele.OnText, func(ctx tele.Context) error {
									answer = strings.Replace(ctx.Text(), "\f", "", 1)
									fmt.Println(answer)
									if answer == yes {
										message = "Выберите дату и время собеседования!\nДля этого введите дату в формате ДД.ММ.ГГГГ!"
										err = ctx.Send(message)
										if err != nil {
											log.Fatal("Error in sending message!")
											return err
										}

										//--------------------8 level----------------------
										nh.kernel.Handle(tele.OnText, func(ctx tele.Context) error {
											selectedDateString := strings.Replace(ctx.Text(), "\f", "", 1)
											selectedDateTime, err := time.Parse(layout, selectedDateString)
											today := time.Now()
											if today.After(selectedDateTime) {
												err = ctx.Send("Неверная дата! Измените дату на корректную!")
												return err
											}
											if err != nil {
												err = ctx.Send("Неверный формат даты! Измените дату на корректную!")
												return err
											}
											if selectedDateTime.Weekday() == time.Saturday || selectedDateTime.Weekday() == time.Sunday {
												err = ctx.Send("Выбранная дата является субботой или воскресеньем. Измените дату на корректную!")
												return err
											}
											result, err := nh.useCase.GetEmptySlots(&selectedDateTime)
											if err != nil {
												log.Fatalf("Error in getting slots from db: %v!", err)
												return err
											}
											var keysInline []tele.Row
											var keyboardInline tele.ReplyMarkup

											if result == nil {
												for i := 0; i < len(slots); i += 3 {
													var btn []tele.Btn
													for j := i; j < i+3; j++ {
														if j >= len(slots) {
															break
														}
														btn = append(btn, keyboardInline.Data(slots[i], slots[i]))
													}
													keysInline = append(keysInline, keyboardInline.Row(btn[:]...))
												}

												keyboardInline.Inline(keysInline[:]...)
												err = ctx.Send("Выберите время", &keyboardInline)
												if err != nil {
													log.Fatal(err)
												}
											} else {
												elements := make(map[string]bool)
												for _, num := range result {
													elements[num] = true
												}
												var newArray []string
												for _, num := range slots {
													if !elements[num] {
														newArray = append(newArray, num)
													}
												}

												for i := 0; i < len(newArray); i += 3 {
													var btn []tele.Btn
													for j := i; j < i+3; j++ {
														if j >= len(newArray) {
															break
														}
														btn = append(btn, keyboardInline.Data(newArray[j], newArray[j]))
													}
													keysInline = append(keysInline, keyboardInline.Row(btn[:]...))
												}

												keyboardInline.Inline(keysInline[:]...)
												err = ctx.Send("Выберите время", &keyboardInline)
												if err != nil {
													log.Fatal(err)
												}

												//--------------------8 level----------------------
												nh.kernel.Handle(tele.OnCallback, func(ctx tele.Context) error {
													userInterviewTime := strings.Replace(ctx.Callback().Data, "\f", "", 1)
													err = nh.useCase.SetInterview(
														&model.SetInterviewParams{
															CandidateChatName: &userChatName,
															Job:               &job,
															CandidateName:     &name,
															DateInterview:     &selectedDateTime,
															Slot:              &userInterviewTime,
														})
													if err != nil {
														return err
													}
													message = fmt.Sprintf("%s, вы успешно записаны на %s в %s на собеседование!\n<b>Просьба не опаздывать и не забывать паспорт!</b>\nКак подниметесь на 3 этаж просьба написать в телеграм @AlinaAssist (https://t.me/AlinaAssist)", name, selectedDateString, userInterviewTime)
													err := ctx.Send(message, &tele.SendOptions{ParseMode: tele.ModeHTML})
													if err != nil {
														log.Fatal("Error in sending message!")
														return err
													}
													ctx.Chat().ID = nh.cfg.Bot.ChatID
													message = fmt.Sprintf("Кандидат %s успешно записан на %s в %s на собеседование!", name, selectedDateString, userInterviewTime)
													err = ctx.Send(message, &tele.SendOptions{ParseMode: tele.ModeHTML})
													if err != nil {
														log.Fatal("Error in sending message!")
														return err
													}
													return nil
												})
											}
											return nil
										})

									} else {
										message = "Спасибо, удачи с выбором работы!"
										err := ctx.Send(message)
										if err != nil {
											log.Fatal("Error in sending message!")
											return err
										}
									}

									return nil
								})
								return nil
							})

							return nil
						})
						return nil
					})
					return nil
				})
			} else {
				message = "Спасибо, удачи с выбором работы!"
				err := ctx.Send(message)
				if err != nil {
					log.Fatal("Error in sending message!")
					return err
				}
			}
			return nil
		})

		return nil
	})

	//--------------------Перенос собеседования----------------------
	nh.kernel.Handle("/change_date_interview", func(ctx tele.Context) error {
		userChatName := ctx.Chat().Username
		message := "Выберите дату и время собеседования!\nДля этого введите дату в формате ДД.ММ.ГГГГ!"
		err := ctx.Send(message)
		if err != nil {
			log.Fatal("Error in sending message!")
			return err
		}

		//--------------------2 level----------------------
		nh.kernel.Handle(tele.OnText, func(ctx tele.Context) error {
			selectedDateString := strings.Replace(ctx.Text(), "\f", "", 1)
			selectedDateTime, err := time.Parse(layout, selectedDateString)
			if err != nil {
				err = ctx.Send("Неверный формат даты!")
			}
			if selectedDateTime.Weekday() == time.Saturday || selectedDateTime.Weekday() == time.Sunday {
				err = ctx.Send("Выбранная дата является субботой или воскресеньем.")
				return err
			}
			result, err := nh.useCase.GetEmptySlots(&selectedDateTime)
			if err != nil {
				log.Fatalf("Error in getting slots from db: %v!", err)
				return err
			}
			var keysInline []tele.Row
			var keyboardInline tele.ReplyMarkup

			if result == nil {
				for i := 0; i < len(slots); i += 3 {
					var btn []tele.Btn
					for j := i; j < i+3; j++ {
						if j >= len(slots) {
							break
						}
						btn = append(btn, keyboardInline.Data(slots[i], slots[i]))
					}
					keysInline = append(keysInline, keyboardInline.Row(btn[:]...))
				}

				keyboardInline.Inline(keysInline[:]...)
				err = ctx.Send("Выберите время", &keyboardInline)
				if err != nil {
					log.Fatal(err)
				}
			} else {
				elements := make(map[string]bool)
				for _, num := range result {
					elements[num] = true
				}
				var newArray []string
				for _, num := range slots {
					if !elements[num] {
						newArray = append(newArray, num)
					}
				}

				for i := 0; i < len(newArray); i += 3 {
					var btn []tele.Btn
					for j := i; j < i+3; j++ {
						if j >= len(newArray) {
							break
						}
						btn = append(btn, keyboardInline.Data(newArray[j], newArray[j]))
					}
					keysInline = append(keysInline, keyboardInline.Row(btn[:]...))
				}

				keyboardInline.Inline(keysInline[:]...)
				err = ctx.Send("Выберите время", &keyboardInline)
				if err != nil {
					log.Fatal(err)
				}

				//--------------------3 level----------------------
				nh.kernel.Handle(tele.OnCallback, func(ctx tele.Context) error {
					userInterviewTime := strings.Replace(ctx.Callback().Data, "\f", "", 1)
					err = nh.useCase.ChangeInterviewDate(
						&model.ChangeInterviewDateParams{
							CandidateChatName: &userChatName,
							DateInterview:     &selectedDateTime,
							Slot:              &userInterviewTime,
						})
					if err != nil {
						return err
					}
					message = fmt.Sprintf("Вы успешно перезаписаны на %s в %s на собеседование!\n<b>Просьба не опаздывать и не забывать паспорт!</b>\nКак подниметесь на 3 этаж просьба написать в телеграм @AlinaAssist (https://t.me/AlinaAssist)", selectedDateString, userInterviewTime)
					err := ctx.Send(message, &tele.SendOptions{ParseMode: tele.ModeHTML})
					if err != nil {
						log.Fatal("Error in sending message!")
						return err
					}
					return nil
				})
			}
			return nil
		})

		return nil
	})
}
