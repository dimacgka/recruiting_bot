package naimInterface

import (
	"dnaim/internal/naim/model"
	"time"
)

type NaimRepositoryI interface {
	GetEmptySlots(date *time.Time) ([]string, error)
	SetInterview(params *model.SetInterviewParams) error
	CheckInterview(params *model.CheckInterviewParams) (*model.CheckInterviewResult, error)
	ChangeInterviewDate(params *model.ChangeInterviewDateParams) error
	GetInterviewByDateAndSlotID(date *time.Time, slotID *int64) ([]model.GetInterviewByDateParams, error)
}
