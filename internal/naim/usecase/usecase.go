package usecase

import (
	"dnaim/config"
	naimInterface "dnaim/internal/naim/interface"
	"dnaim/internal/naim/model"
	"dnaim/pkg/logger"
	"time"
)

type NaimUseCase struct {
	NaimRepo naimInterface.NaimRepositoryI
	logger   logger.Logger
	cfg      *config.Config
}

func NewNaimUseCase(ApiGoRepo naimInterface.NaimRepositoryI, logger logger.Logger, cfg *config.Config) naimInterface.NaimUseCaseI {
	return &NaimUseCase{
		NaimRepo: ApiGoRepo,
		logger:   logger,
		cfg:      cfg,
	}
}

func (nus *NaimUseCase) GetEmptySlots(date *time.Time) ([]string, error) {
	result, err := nus.NaimRepo.GetEmptySlots(date)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (nus *NaimUseCase) SetInterview(params *model.SetInterviewParams) error {
	err := nus.NaimRepo.SetInterview(params)
	if err != nil {
		return err
	}

	return nil
}

func (nus *NaimUseCase) CheckInterview(params *model.CheckInterviewParams) (*model.CheckInterviewResult, error) {
	result, err := nus.NaimRepo.CheckInterview(params)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (nus *NaimUseCase) ChangeInterviewDate(params *model.ChangeInterviewDateParams) error {
	err := nus.NaimRepo.ChangeInterviewDate(params)
	if err != nil {
		return err
	}

	return nil
}

func (nus *NaimUseCase) GetInterviewByDateAndSlotID(date *time.Time, slotID *int64) ([]model.GetInterviewByDateParams, error) {
	result, err := nus.NaimRepo.GetInterviewByDateAndSlotID(date, slotID)
	if err != nil {
		return nil, err
	}

	return result, nil
}
