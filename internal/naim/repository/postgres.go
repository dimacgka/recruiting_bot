package repository

import (
	naimInterface "dnaim/internal/naim/interface"
	"dnaim/internal/naim/model"
	"dnaim/pkg/logger"
	"github.com/jmoiron/sqlx"
	"time"
)

type NaimGoRepo struct {
	db     *sqlx.DB
	logger *logger.Logger
}

func NewNaimGoRepo(db *sqlx.DB, logger *logger.Logger) naimInterface.NaimRepositoryI {
	return &NaimGoRepo{
		db:     db,
		logger: logger,
	}
}

func (ngr *NaimGoRepo) GetEmptySlots(date *time.Time) ([]string, error) {
	query := `
	SELECT s.name
	FROM interview i
	JOIN slots s ON i.slots_id = s.id
	WHERE DATE(date_interview) = DATE($1);`

	result := make([]string, 0)

	if err := ngr.db.Select(&result, query, date); err != nil {
		ngr.logger.Errorf("GetEmptySlots: %v", err)
		return nil, err
	}

	return result, nil
}

func (ngr *NaimGoRepo) SetInterview(params *model.SetInterviewParams) error {
	query := `
	INSERT INTO interview (slots_id, date_interview, candidate_name, job, candidate_chat_name)
	VALUES ((SELECT id FROM slots WHERE name = $1), $2, $3, LOWER($4), $5);`

	_, err := ngr.db.Exec(query, params.Slot, params.DateInterview, params.CandidateName, params.Job, params.CandidateChatName)
	if err != nil {
		ngr.logger.Errorf("SetInterview: %v", err)
		return err
	}

	return nil
}

func (ngr *NaimGoRepo) CheckInterview(params *model.CheckInterviewParams) (*model.CheckInterviewResult, error) {
	query := `
	SELECT s.name AS slot, i.date_interview, i.candidate_name
	FROM interview i
	JOIN slots s on i.slots_id = s.id
	WHERE i.candidate_chat_name = $1 AND job = LOWER($2);`

	var result []model.CheckInterviewResult

	if err := ngr.db.Select(&result, query, params.CandidateChatName, params.Job); err != nil {
		ngr.logger.Errorf("GetEmptySlots: %v", err)
		return nil, err
	}

	if result == nil {
		return nil, nil
	}

	return &result[0], nil
}

func (ngr *NaimGoRepo) ChangeInterviewDate(params *model.ChangeInterviewDateParams) error {
	query := `
	UPDATE interview
	SET slots_id = (SELECT id FROM slots id WHERE name = $1),
		date_interview = $2
	WHERE candidate_chat_name = $3;`

	_, err := ngr.db.Exec(query, params.Slot, params.DateInterview, params.CandidateChatName)
	if err != nil {
		ngr.logger.Errorf("ChangeInterviewDate: %v", err)
		return err
	}

	return nil
}

func (ngr *NaimGoRepo) GetInterviewByDateAndSlotID(date *time.Time, slotID *int64) ([]model.GetInterviewByDateParams, error) {
	query := `
	SELECT s.name AS slot, i.id AS id, COALESCE(i2.name, '') AS interviewer, i.candidate_name, st.name AS status, COALESCE(i.candidate_chat_name, '') AS candidate_chat_name,
	       i.date_interview::DATE::VARCHAR, i.is_held, i.is_approve, COALESCE(i.job, '') AS job, COALESCE(i.description, '') AS description, i2.chat_id AS chat_id
	FROM interview i
	JOIN slots s ON i.slots_id = s.id
	JOIN status st ON i.status_id = st.id
	LEFT JOIN interviewer i2 ON i.interviewer_id = i2.id
	WHERE i.date_interview = $1 AND i.slots_id = $2
	ORDER BY i.date_interview;`

	var result []model.GetInterviewByDateParams

	if err := ngr.db.Select(&result, query, date, slotID); err != nil {
		ngr.logger.Errorf("GetEmptySlots: %v", err)
		return nil, err
	}

	if result == nil {
		return nil, nil
	}

	return result, nil
}
