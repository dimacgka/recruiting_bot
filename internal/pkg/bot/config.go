package config

type BotConfig struct {
	Token      string `json:"token"`
	AppVersion string `json:"appVersion"`
	TimeOut    int    `json:"timeOut"`
	ChatID     int64  `json:"chatID"`
}

func (bc *BotConfig) ParseConfig() error {
	return nil
}
