package connection

import (
	"dnaim/config"
	naim "dnaim/internal/naim/delivery"
	"dnaim/internal/naim/repository"
	"dnaim/internal/naim/usecase"
	"dnaim/pkg/logger"
	"dnaim/pkg/storage/relational"
)

func (t *TelegramBot) MapHandlers(cfg *config.Config) error {
	logger := logger.InitLogger(cfg)

	//dHttpClient := dhttp.NewHttpClient(cfg, logger)

	naimGoDB := relational.InitPostgres(cfg, logger)
	naimGoRepo := repository.NewNaimGoRepo(naimGoDB, logger)

	naimUseCase := usecase.NewNaimUseCase(naimGoRepo, *logger, cfg)

	clientStatisticHandler := naim.NewNaimHandler(naimUseCase, t.core, *cfg)
	clientStatisticHandler.Attach()

	return nil
}
